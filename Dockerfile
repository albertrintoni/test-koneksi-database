FROM php:7.2-apache

COPY ./html /var/www/html

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

EXPOSE 80